#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, RawTextHelpFormatter
import multiprocessing
import random
import itertools
from pathlib import Path
import operator

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

def load_graph(file_path: Path) -> nx.graph:
	if ".graphml" in file_path.suffixes:
		return nx.read_graphml(str(file_path))
	else:
		return nx.read_edgelist(str(file_path))


def filter_subgraph(graph: nx.graph, threshold: int) -> nx.graph:
	"""
	Remove all subgraphs with a nodecount < threshold * max nodecount
	"""
	subgraphs = [graph.subgraph(c).copy() for c in nx.connected_components(graph)]
	subgraph_length_list = [len(g) for g in subgraphs]
	
	if len(subgraphs) == 0:
		return graph # Do nothing if no other subgraphs are present
	
	# Discard small subgraphs
	threshold = max(subgraph_length_list) * threshold
	#print("Deleting all subgraphs with a nodecount < {}".format(threshold))
	graph_list = [g for g in subgraphs if len(g) >= threshold]
	
	# Create new filtered graph
	G = nx.Graph()
	for graph in graph_list:
		G = nx.compose(G, graph)
		
	#print("Removed {} nodes out of {}".format(len(subgraphs) - len(graph_list), len(subgraphs)))
		
	return G
	
def analyze_graph(G: nx.graph):
	degree_list = G.degree().values()
	print("Max degree {} Min degree {} Avg degree {}".format(max(degree_list), min(degree_list), sum(degree_list)/len(degree_list)))
	
	density = nx.density(G)
	print("Density of Graph is {}".format(density))

	clustering_coefficient = nx.average_clustering(G)
	print("Average clustering coefficient: {}".format(clustering_coefficient))
	# TODO: distribution of clustering
	
	#k_components = nx.k_components(G)
	#print("K components {}".format(k_components))
	
	connectivity = nx.node_connectivity(G)
	print("We need to remove {} nodes to break the graph".format(connectivity))
	
	return [density, connectivity, clustering_coefficient, degree_list]
	
def greedy_remove(graph: nx.graph):
	# get max degree node
	degree_dict = graph.degree()
	max_degree_id = max(degree_dict, key=operator.itemgetter(1))[0]
	# remove max node
	graph.remove_node(max_degree_id)
	
def random_remove(graph: nx.graph):
	nodes = graph.nodes()
	random_node = random.choice(list(nodes))
	graph.remove_node(random_node)
	
	
def attack_step(graph: nx.graph, greedy : bool):
	# TODO: copy graph or make filer_subgraph edit in place
	if greedy:
		greedy_remove(graph)
	else:
		random_remove(graph)
	
	#num_networks = len(list(nx.connected_component_subgraphs(graph)))
	graph = filter_subgraph(graph, 1)
	#max_num_nodes = len(graph.nodes())
	
	return graph
	#return [num_networks, max_num_nodes]
	
def attack(graph: nx.graph, greedy: bool):
	local_graph = graph.to_undirected()
	
	total_nodes = len(local_graph.nodes())
	disconnected_nodes = 0
	removed_nodes = 0
	result_list = []
	
	while (disconnected_nodes + 0.0001) / total_nodes < 0.999999: # Repeat until the network is dead
		local_graph = attack_step(local_graph, greedy)
		removed_nodes += 1
		num_nodes = local_graph.order()
		disconnected_nodes = total_nodes - num_nodes
		
		#avg_shortest_path = nx.average_shortest_path_length(local_graph)
		density = nx.density(local_graph)
		avg_clustering = 0
		# avg_clustering for every step takes way too long
		#try:
		#	avg_clustering = nx.average_clustering(local_graph)
		#except:
		#	pass
		degrees = list(map(operator.itemgetter(1), graph.degree()))
		avg_degree = np.mean(degrees)
		median_degree = np.median(degrees)
		
		
		
		result_list.append((disconnected_nodes/total_nodes, removed_nodes/total_nodes, density, avg_clustering, avg_degree, median_degree, num_nodes))
		print("\rRemoved nodes {}                 ".format(disconnected_nodes / total_nodes), end="")
		
	print("\nEnd disconnected {} total {} removed {}".format(disconnected_nodes, total_nodes, removed_nodes))
		
	return result_list
	
	
def thread_helper_attack(input_file: Path, greedy: bool, output_dir: str):
	G = load_graph(input_file)
	res = attack(G, greedy)
	
	if output_dir != None:
		if not output_dir.exists():
			output_dir.mkdir()
		filename = "{}_{}".format(input_file.name, "greedy" if greedy else "random")
		print("Saving results as {}".format(output_dir / filename))
		np.save(str(output_dir / filename), res)

def plot_results(results):
	fig, ((ax_remaining_nodes)) = plt.subplots(1, 1)

	
	for result in results:
		print(np.transpose(result[2]))
		ax_remaining_nodes.plot(np.transpose(result[2])[1], np.transpose(result[2])[0], label="{} {}".format(result[0], "greedy" if result[1] else "random"))
	
	ax_remaining_nodes.set_ylabel("Isolated nodes")
	ax_remaining_nodes.set_xlabel("Removed nodes")
	
	ax_remaining_nodes.legend(loc=0)
	ax_remaining_nodes.grid()
	plt.show()
	

def main(argv=None):
	if argv is None:
		argv = sys.argv
	else:
		sys.argv.extend(argv)
		
	parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
	parser.add_argument("-i", "--input", dest="input_files", help="Input files", type=Path, nargs="+", default=[])
	parser.add_argument("-o", "--output", dest="output_dir", help="Output dir", type=Path, default=None)
	parser.add_argument("-r", "--result_files", dest="result_files", help="Result files", type=Path, nargs="+", default=[])
	parser.add_argument("-t", "--threads", dest="thread_count", help="Number of maximum threads to use", type=int, default = multiprocessing.cpu_count())
	#parser.add_argument("-t", "--threshold", dest="discard_threshold", help="Threshold of maximum network size to start discarding smaller networks", type=int, default=0.1)
	
	args = parser.parse_args()
	
	if len(args.input_files) > 0:
		with multiprocessing.Pool(args.thread_count) as p:
			p.starmap(thread_helper_attack, itertools.product(args.input_files, [True,False],[args.output_dir] ))
		
	if len(args.result_files) > 0:
		load_results = []
		for file in args.result_files:
			base_name = file.name
			tokens = base_name.split("_")
			title = "".join(tokens[:-1])
			greedy = True if tokens[-1] == "greedy.npy" else False
			data = np.load(str(file))
			load_results.append([title, greedy, data])
		plot_results(load_results)
	
	
if __name__ == "__main__":
	main()
	
