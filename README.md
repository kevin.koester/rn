# Setup

```
python3 -m venv ./venv
source ./venv/bin/activate
pip install -r requirements.txt
./analyze.py -i <input file> [-t <threshold>]
```
